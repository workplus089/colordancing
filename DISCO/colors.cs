﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

using System.Threading;
namespace DISCO
{
    class colors
    {
        private bool run = true;
        private int time;
        public int Time
        {
            set { time = value; }
            get { return time; }
        }
        public bool Run
        {
            set { run = value; }
            get { return run; }
        }
        static List<Color> colist = new List<Color>
            { Color.Red, Color.Salmon,Color.Beige,Color.Firebrick,Color.AliceBlue,
        Color.AntiqueWhite,
        Color.Aqua,Color.Aquamarine,Color.Azure,
               Color.Beige,
                 Color.Bisque,
                 Color.Black,
                 Color.BlanchedAlmond,
               Color.Blue,
              Color.BlueViolet,
                 Color.Brown,
                 Color.BurlyWood,
                Color.CadetBlue,
                 Color.Chartreuse,
                 Color.Chocolate,
                 Color.Coral,
                 Color.CornflowerBlue,
                Color.Cornsilk,
                Color.Crimson,
                 Color.Cyan,
                 Color.DarkBlue,
                Color.DarkCyan,
                 Color.DarkGoldenrod,
                 Color.DarkGray,
                 Color.DarkGreen,
                 Color.DarkKhaki,
                 Color.DarkMagenta,
                 Color.DarkOliveGreen,
               Color.DarkOrange,
                 Color.DarkOrchid,
                 Color.DarkRed,
                 Color.DarkSalmon,
                 Color.DarkSeaGreen,
                 Color.DarkSlateBlue,
                 Color.DarkSlateGray,
                 Color.DarkTurquoise,
                 Color.DarkViolet,
                Color.DeepPink,
                 Color.DeepSkyBlue,
                 Color.DimGray,
                Color.DodgerBlue,
                Color.Firebrick,
                Color.FloralWhite,
                 Color.ForestGreen,
                 Color.Fuchsia,
                 Color.Gainsboro,
                 Color.GhostWhite,
                 Color.Gold,
                 Color.Goldenrod,
                Color.Gray,
                 Color.Green,
                 Color.GreenYellow,
                 Color.Honeydew,
                 Color.HotPink,
                 Color.IndianRed,
                 Color.Indigo,
                 Color.Ivory,
                 Color.Khaki, Color.Tomato, Color.Transparent,
                 Color.Turquoise,
                 Color.Violet,
                 Color.Wheat,
                 Color.White,
                 Color.WhiteSmoke,
                Color.Yellow,
                 Color.YellowGreen,
                Color.Purple, Color.Lavender,
        Color.LavenderBlush,
                 Color.LawnGreen,
                Color.LemonChiffon,
                 Color.LightBlue,
                Color.LightCoral,
                Color.LightCyan,
                Color.LightGoldenrodYellow,
                Color.LightGray,
                 Color.LightGreen,
                 Color.PaleTurquoise,
        Color.PaleVioletRed,
                 Color.PapayaWhip,
                 Color.PeachPuff,
                Color.Peru,
                 Color.Pink,
                 Color.Plum,
                 Color.PowderBlue,
                 Color.Purple,
                Color.Red,
                 Color.RosyBrown,
                 Color.RoyalBlue,
                 Color.SaddleBrown,
                 Color.Salmon,
                 Color.SandyBrown,
                Color.SeaGreen,
                Color.SeaShell,
                 Color.Sienna,
                 Color.Silver,
                 Color.SkyBlue,
                 Color.SlateBlue,
                 Color.SlateGray,
                 Color.Snow,
                 Color.SpringGreen,
                 Color.SteelBlue,
                 Color.Tan,
                 Color.Teal,
                Color.Thistle,Color.LightPink,
        };                
        static Random rnd = new Random();
        static Random crnd = new Random();
        public Color make_color()
        {
            try
            {
                int red = crnd.Next(1, 25) * 10;
                int blue = crnd.Next(1, 25) * 10;
                int green = crnd.Next(1, 25) * 10;
                Color color = Color.FromArgb(red, green, blue);
                return color;
            }
            catch (Exception)
            {
                return Color.Red;
            }
        }
        public void regular()
        {
            try
            {


                while (Run)
                {
                    Thread.Sleep(Time);
                    //Form1.ActiveForm.BackColor = make_color();

                }
            }
            catch (Exception)
            {
                Thread.Sleep(1000);
                Form1.ActiveForm.BackColor = make_color();

            }
        }        
        
    }
}
