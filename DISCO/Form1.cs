﻿using System;
using System.Threading;
using System.Windows.Forms;
using System.Drawing;

namespace DISCO
{
    public partial class Form1 : Form
    {
        colors colorpick = new colors();
    

        public Form1()
        {
            InitializeComponent();
        }
       

        

        
        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox3.Checked)
            {
                FormBorderStyle = FormBorderStyle.None;
                WindowState = FormWindowState.Maximized;
            }
            if (checkBox3.CheckState == CheckState.Unchecked)
            {
                FormBorderStyle = FormBorderStyle.FixedSingle;
                WindowState = FormWindowState.Normal;

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
           colorpick.Run = false;            
           System.Environment.Exit(0);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            CheckForIllegalCrossThreadCalls = false;
            backgroundWorker1.RunWorkerAsync();
            ThreadStart ts = new ThreadStart(colorpick.regular);
            Thread TRD = new Thread(ts);
            TRD.Start();
            //ThreadStart ts = new ThreadStart(QWE);
            //Thread TRD = new Thread(ts);
            //TRD.Start();
            //ThreadStart ts1 = new ThreadStart(ALI);
            //Thread TRD1 = new Thread(ts1);
            //TRD1.Start();
        }

        private void progressBar1_Click(object sender, EventArgs e)
        {
            Point point = progressBar1.PointToClient(Cursor.Position);
            progressBar1.Value = progressBar1.Minimum + (progressBar1.Maximum - progressBar1.Minimum) * point.X / progressBar1.Width;
            colorpick.Time = progressBar1.Value * 5 + 500;
            label2.Text = colorpick.Time.ToString()+" ms";

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            //colorpick.Time = progressBar1.Value * 5 + 500;
            //Thread.Sleep(600);

            //ThreadStart ts = new ThreadStart(colorpick.regular);
            //Thread TRD = new Thread(ts);
            //TRD.Start();
            //ThreadStart ts1 = new ThreadStart(colorpick.regular);
            //Thread TRD1 = new Thread(ts1);
            //TRD1.Start();
        }

        private void backgroundWorker1_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            colorpick.Time = progressBar1.Value * 5 + 500;
            Thread.Sleep(600);
            while (colorpick.Run)
            {
                Thread.Sleep(colorpick.Time);
                Form1.ActiveForm.BackColor = colorpick.make_color();

            }


        }
    }
}